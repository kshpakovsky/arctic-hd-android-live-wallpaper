package ru.atonica.arctic;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.extension.ui.livewallpaper.BaseLiveWallpaperService;
import org.andengine.opengl.util.GLState;

import java.io.IOException;


public class ArcticWallpaperService extends BaseLiveWallpaperService {
	
	public static final float CAMERA_HEIGHT = Constants.SCENE_HEIGHT;
	
	private Camera mCamera;
	private float mXOffset = 0;
	private boolean mIsPreview = true;
    private WallpaperSettings.SceneType currentSceneType;
    private boolean firstInitialised = false;

	@Override
	public EngineOptions onCreateEngineOptions() {
        WallpaperSettings.getInstance().init(this);

		final float width = getResources().getDisplayMetrics().widthPixels;
		final float height = getResources().getDisplayMetrics().heightPixels;
		final float idx = width / height;
		final float newWidth = Constants.SCENE_HEIGHT * idx;
		mCamera = new Camera(0f, 0f, newWidth, CAMERA_HEIGHT);
        WallpaperSettings.getInstance().camera = mCamera;
        EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_SENSOR, new RatioResolutionPolicy(newWidth, CAMERA_HEIGHT), mCamera);
        engineOptions.getRenderOptions().setDithering(true);
//        engineOptions.getRenderOptions().getConfigChooserOptions().setRequestedMultiSampling(true);
		return engineOptions;
	}

	@Override
	public void onCreateResources(
			OnCreateResourcesCallback pOnCreateResourcesCallback)
			throws IOException {

        ResourceManager.getInstance().setup(getEngine(), getApplicationContext());
        firstInitialised = true;

		pOnCreateResourcesCallback.onCreateResourcesFinished();
	}

	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback)
			throws IOException {

		pOnCreateSceneCallback.onCreateSceneFinished(new Scene());
	}

	@Override
	public void onSurfaceChanged(GLState pGLState, int pWidth, int pHeight) {
		pGLState.enableDither();
		final float idx = (float)pWidth / pHeight;
		final float width = Constants.SCENE_HEIGHT * idx;
		mCamera.set(0, 0, width, Constants.SCENE_HEIGHT);
		
		if(mIsPreview) {
			mCamera.setCenter(Constants.SCENE_WIDTH / 2, mCamera.getCenterY());
		}
		else {
			final float offset = Constants.SCENE_WIDTH * mXOffset - mCamera.getWidth() * mXOffset;
			mCamera.setCenter(offset + mCamera.getWidth() / 2, mCamera.getCenterY());
		}
			
		super.onSurfaceChanged(pGLState, pWidth, pHeight);
	}
	
	@Override
	protected void onOffsetsChanged(float pXOffset, float pYOffset,
			float pXOffsetStep, float pYOffsetStep, int pXPixelOffset,
			int pYPixelOffset) {
        mIsPreview = pXOffsetStep == 0f;
		mXOffset = pXOffset;
		final float offset = Constants.SCENE_WIDTH * pXOffset - mCamera.getWidth() * pXOffset;
		mCamera.setCenter(offset + mCamera.getWidth() / 2, mCamera.getCenterY());
		
		super.onOffsetsChanged(pXOffset, pYOffset, pXOffsetStep, pYOffsetStep,
				pXPixelOffset, pYPixelOffset);
	}
	
	@Override
	public void onPopulateScene(Scene pScene,
			OnPopulateSceneCallback pOnPopulateSceneCallback) throws IOException {

//        SceneManager.getInstance().showDayScene();
        WallpaperSettings.getInstance().setSettingsChangedListener(SceneManager.getInstance());
		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}

    @Override
    public synchronized void onResumeGame() {

        if(firstInitialised) {
            WallpaperSettings.SceneType settingsSceneType = WallpaperSettings.getInstance().getSceneType();
            WallpaperSettings.SceneType localSceneType = WallpaperSettings.SceneType.DAY;
            boolean sceneTypeChanged = false;

            if(settingsSceneType == WallpaperSettings.SceneType.AUTO) {
                if(WallpaperSettings.getInstance().isAutoSceneTypeChanged(currentSceneType)) {
                    localSceneType = WallpaperSettings.getInstance().getAutoSceneType();
                    sceneTypeChanged = true;
                }
            } else {
                if(currentSceneType != settingsSceneType) {
                    localSceneType = settingsSceneType;
                    sceneTypeChanged = true;
                }
            }
            if(sceneTypeChanged) {
                switch (localSceneType) {
                    case DAY:
                        SceneManager.getInstance().showDayScene();
                        currentSceneType = WallpaperSettings.SceneType.DAY;
                        break;
                    case NIGHT:
                        SceneManager.getInstance().showNightScene();
                        currentSceneType = WallpaperSettings.SceneType.NIGHT;
                        break;
                    default:
                        SceneManager.getInstance().showDayScene();
                        currentSceneType = WallpaperSettings.SceneType.DAY;
                }
            }
        }
        super.onResumeGame();
    }
}
