package ru.atonica.arctic;

import android.test.suitebuilder.annotation.Smoke;
import org.andengine.entity.scene.background.IBackground;
import org.andengine.entity.sprite.AnimatedSprite;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 26.03.13
 * Time: 15:54
 * To change this template use File | Settings | File Templates.
 */
public class NightScene extends ManagedScene implements Snow.ISnowEvents {

    private NightScene thisNightScene = this;
    private BirdsFlock birdsFlock;
    private Snow snow;
    private Aurora aurora;
    private Fan fan;
    private AnimatedSprite smoke;

    @Override
    public void onHideScene() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onLoadScene() {
        ObjectFactory.setSceneType(WallpaperSettings.SceneType.NIGHT);
        ResourceManager.loadNightResources();

        // water >>
        this.attachChild(ObjectFactory.createWater());
        // << water

        this.attachChild(ObjectFactory.createNightSky());

        aurora = ObjectFactory.createAurora();
        this.attachChild(aurora);

        this.attachChild(ObjectFactory.createNightHills());

        this.attachChild(ObjectFactory.createGrampus());

        // clouds >>
        this.attachChild(ObjectFactory.createMediumCloud(7f));
        this.attachChild(ObjectFactory.createBigCloud(6f));
        // << clouds

        // fan >>
        fan = ObjectFactory.createFan();
        this.attachChild(fan);
        // << fan

        this.attachChild(ObjectFactory.createAirship());

        // smoke >>
        smoke = ObjectFactory.createSmoke();
        this.attachChild(smoke);
        // << smoke

        birdsFlock = ObjectFactory.createBirdsFlock(WallpaperSettings.getInstance().getBirdsInFlockCount());
        this.attachChild(birdsFlock);

        snow = ObjectFactory.createSnow(this);
        this.attachChild(snow);
    }

    @Override
    public void onShowScene() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onUnloadScene() {
        ResourceManager.getInstance().engine.runOnUpdateThread(new Runnable() {
            @Override
            public void run() {
                thisNightScene.detachChildren();
                thisNightScene.clearEntityModifiers();
                thisNightScene.clearUpdateHandlers();
                ResourceManager.unloadNightResources();
            }
        });
    }

    @Override
    public void onSettingsChanged(String key) {
        if(key.equals(WallpaperSettings.BIRDS_IN_FLOCK_KEY)) {
            if(birdsFlock != null) {
                birdsFlock.detachSelf();
            }
            birdsFlock = ObjectFactory.createBirdsFlock(WallpaperSettings.getInstance().getBirdsInFlockCount());
            this.attachChild(birdsFlock);
        }

        if(key.equals(WallpaperSettings.SNOW_ENABLED_KEY) || key.equals(WallpaperSettings.SNOW_FREQUENCY_KEY)) {
            if(snow != null) {
                snow.setIsSnowy(WallpaperSettings.getInstance().isSnowEnabled());
            }
        }
    }

    @Override
    public void snowStarted() {
        aurora.stopAnimation(0);
        fan.setAngularVelocity(WallpaperSettings.FAN_VELOCITY_FAST);
        smoke.animate(WallpaperSettings.SMOKE_FRAME_DURATION_FAST);
    }

    @Override
    public void snowStopped() {
        aurora.animateInCicle(WallpaperSettings.AURORA_FRAME_DURATION);
        fan.setAngularVelocity(WallpaperSettings.FAN_VELOCITY_NORMAL);
        smoke.animate(WallpaperSettings.SMOKE_FRAME_DURATION_NORMAL);
    }
}
