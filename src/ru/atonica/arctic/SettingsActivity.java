package ru.atonica.arctic;

import android.*;
import android.R;
import android.app.Activity;
import android.os.Bundle;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 21.03.13
 * Time: 13:39
 * To change this template use File | Settings | File Templates.
 */
public class SettingsActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();
    }
}