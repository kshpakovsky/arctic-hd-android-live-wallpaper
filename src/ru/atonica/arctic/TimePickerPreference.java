package ru.atonica.arctic;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TimePicker;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 22.03.13
 * Time: 16:13
 * To change this template use File | Settings | File Templates.
 */
public class TimePickerPreference extends DialogPreference {
    private int lastHour;
    private int lastMinutes;
    private TimePicker timePicker;
    private int fromHour = 0, toHour = 23;

    public TimePickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setPositiveButtonText("OK");
        setNegativeButtonText("Cancel");
        setDialogIcon(null);
    }

    public void setHoursLimit(int fromHour, int toHour) {
        this.fromHour = fromHour;
        this.toHour = toHour;
    }

    @Override
    protected View onCreateDialogView() {
        timePicker = new TimePicker(getContext());
        timePicker.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
        timePicker.setIs24HourView(true);
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                if(hourOfDay < fromHour) {
                    view.setCurrentHour(toHour);
                }
                if(hourOfDay > toHour) {
                    view.setCurrentHour(fromHour);
                }
            }
        });
        return timePicker;
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if(positiveResult) {
            timePicker.clearFocus();
            persistString(timePicker.getCurrentHour().toString() + ":" + timePicker.getCurrentMinute().toString());
        }
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        String pt;
        if(restorePersistedValue) {
            pt = getPersistedString("00:00");
        } else {
            pt = defaultValue.toString();
        }
        persistString(pt);
        lastHour = Integer.parseInt(pt.split(":")[0]);
        lastMinutes = Integer.parseInt(pt.split(":")[1]);
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getString(index);
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);

        timePicker.setCurrentHour(lastHour);
        timePicker.setCurrentMinute(lastMinutes);
    }
}
