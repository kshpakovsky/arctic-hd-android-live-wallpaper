package ru.atonica.arctic;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.*;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;
import org.andengine.util.modifier.IModifier;
import org.andengine.util.modifier.ease.EaseSineInOut;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 27.03.13
 * Time: 11:03
 * To change this template use File | Settings | File Templates.
 */
public class Airship extends Sprite implements IModifier.IModifierListener<IEntity> {

    private final static float FLY_DURATION = 240;
    private final static float HANG_DURATION = 10;
    private final static float INITIAL_SCALE = 0.5f;

    public Airship(ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        this(0, 0, pTextureRegion, pVertexBufferObjectManager);
        this.setPosition(-this.getWidth()/2, Constants.SCENE_HEIGHT+this.getHeight()/2);
    }

    public Airship(float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
        this.setScale(INITIAL_SCALE);
        this.registerEntityModifier(createModifierWithRandomStartTime());
    }

    private SequenceEntityModifier createModifierWithRandomStartTime() {
        SequenceEntityModifier modifier = new SequenceEntityModifier(
                new DelayModifier(MathUtils.random(30, 90)),
                new ParallelEntityModifier(
                        new MoveModifier(FLY_DURATION, -this.getWidth()/2, Constants.SCENE_HEIGHT+this.getHeight()/2, 915, Constants.SCENE_HEIGHT-196, EaseSineInOut.getInstance()),
                        new ScaleModifier(FLY_DURATION-5, 0.5f, 1f)
                ),
                new DelayModifier(HANG_DURATION),
                new ParallelEntityModifier(
                        new SequenceEntityModifier(
                                new DelayModifier(5),
                                new ScaleModifier(FLY_DURATION-5, 1f, 0.5f)
                        ),
                        new MoveModifier(FLY_DURATION, 915, Constants.SCENE_HEIGHT-196, Constants.SCENE_WIDTH+this.getWidth()/2, Constants.SCENE_HEIGHT+this.getHeight()/2, EaseSineInOut.getInstance())
                )
        );

        modifier.setAutoUnregisterWhenFinished(true);
        modifier.addModifierListener(this);
        return modifier;
    }

    @Override
    public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
        pItem.registerEntityModifier(createModifierWithRandomStartTime());
    }

    @Override
    public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {

    }
}
