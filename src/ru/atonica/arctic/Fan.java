package ru.atonica.arctic;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 26.03.13
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */
public class Fan extends Sprite {

    private float angularVelocity = WallpaperSettings.FAN_VELOCITY_NORMAL;
    private PhysicsHandler physicsHandler;

    public Fan(float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
        physicsHandler = new PhysicsHandler(this);
        registerUpdateHandler(physicsHandler);
        physicsHandler.setAngularVelocity(angularVelocity);
    }

    public void setAngularVelocity(final float pAngularVelocity) {
        angularVelocity = pAngularVelocity;
        physicsHandler.setAngularVelocity(angularVelocity);
    }
}
