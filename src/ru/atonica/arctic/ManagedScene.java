package ru.atonica.arctic;

import org.andengine.entity.scene.Scene;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 26.03.13
 * Time: 15:29
 * To change this template use File | Settings | File Templates.
 */
public abstract class ManagedScene extends Scene {

    public boolean isLoaded = false;

    public ManagedScene() {

    }

    public void onLoadManagedScene() {
        if(!isLoaded) {
            onLoadScene();
            isLoaded = true;
            this.setIgnoreUpdate(true);
        }
    }

    public void onUnloadManagedScene() {
        if(isLoaded) {
            onUnloadScene();
            isLoaded = false;
        }
    }

    public void onShowManagedScene() {
        this.setIgnoreUpdate(false);
        onShowScene();
    }

    public void onHideManagedScene() {
        onHideScene();
        this.setIgnoreUpdate(true);
    }

    public void onSettingsChangedManagedScene(String key) {
        onSettingsChanged(key);
    }

    public abstract void onLoadScene();
    public abstract void onShowScene();
    public abstract void onUnloadScene();
    public abstract void onHideScene();
    public abstract void onSettingsChanged(String key);
}
