package ru.atonica.arctic;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 09.04.13
 * Time: 11:08
 * To change this template use File | Settings | File Templates.
 */
public class Grampus extends AnimatedSprite implements AnimatedSprite.IAnimationListener {
    private PhysicsHandler physicsHandler;

    public Grampus(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
        physicsHandler = new PhysicsHandler(this);
        physicsHandler.setEnabled(false);
        physicsHandler.setVelocityX(-25f);
        this.registerUpdateHandler(physicsHandler);
    }

    public void start() {
        animate(100, this);
        physicsHandler.setEnabled(true);
        setVisible(true);
    }

    public void startInRandomPosition() {
        setPosition(MathUtils.random(WallpaperSettings.getInstance().camera.getXMin(), WallpaperSettings.getInstance().camera.getXMax()), MathUtils.random(100, 300));
        start();
    }

    public void stop() {
        setVisible(false);
        physicsHandler.setEnabled(false);
        stopAnimation(0);
    }

    @Override
    public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
    }

    @Override
    public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
    }

    @Override
    public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
    }

    @Override
    public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) {
        stop();
        this.registerUpdateHandler(new TimerHandler(MathUtils.random(15f, 40f), new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                startInRandomPosition();
            }
        }));
    }
}
