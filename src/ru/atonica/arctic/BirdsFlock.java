package ru.atonica.arctic;

import android.util.Log;
import org.andengine.entity.Entity;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.util.math.MathUtils;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 27.03.13
 * Time: 13:45
 * To change this template use File | Settings | File Templates.
 */
public class BirdsFlock extends Entity implements Bird.IBirdFinishedListener {

    private int birdsCount = 7;
    private int pathNumber = 0;
    private int birdsDelay = MathUtils.random(30, 90);
    private ITextureRegion textureRegion;
    private Bird[] birds;
    private int birdsFinished = 0;
    private float randomXFrom = -25f;
    private float randomXTo = 25f;
    private float randomYFrom = -15f;
    private float randomYTo = 15f;

    public BirdsFlock(int pBirdsCount, int pBirdsDelay, int pPathNumber, ITiledTextureRegion pTextureRegion) {
        birdsCount = pBirdsCount;
        birdsDelay = pBirdsDelay;
        pathNumber = pPathNumber;
        textureRegion = pTextureRegion;
        if(pBirdsCount > 20) {
            randomXFrom = -35f;
            randomXTo = 35f;
            randomYFrom = -25f;
            randomYTo = 25f;
        }
        birds = new Bird[birdsCount];
        for(int i=0; i<birdsCount; i++) {
            birds[i] = new Bird(-100, -100, birdsDelay, pathNumber, pTextureRegion, ResourceManager.getInstance().engine.getVertexBufferObjectManager());
            birds[i].setPathNumber(pathNumber);
            birds[i].setBirdDelay(birdsDelay);
            birds[i].setBirdFinishedListener(this);
            birds[i].setRandomize(randomXFrom, randomXTo, randomYFrom, randomYTo);
            this.attachChild(birds[i]);
        }
        restartBirds();
    }

    private void restartBirds() {
        for(Bird bird: birds) {
            bird.setBirdDelay(birdsDelay);
            bird.setPathNumber(pathNumber);
            bird.startBird();
        }
    }

    @Override
    public void setAlpha(float pAlpha) {
        super.setAlpha(pAlpha);
        for(Bird bird: birds) {
            bird.setAlpha(pAlpha);
        }
    }

    private synchronized void incrementBirdsFinished() {
        birdsFinished++;
//        Log.i("Arctic", "birdsFinished:" + birdsFinished);
    }

    private synchronized int getBirdsFinished() {
        return birdsFinished;
    }

    @Override
    public void onBirdFinished() {
        incrementBirdsFinished();
        if(getBirdsFinished() == birdsCount) {
            birdsFinished = 0;
            birdsDelay = MathUtils.random(30, 90);
            pathNumber = 0;
            restartBirds();
        }
    }
}
