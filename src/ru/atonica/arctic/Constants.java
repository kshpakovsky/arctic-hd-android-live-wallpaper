package ru.atonica.arctic;

public class Constants {
	public static final boolean Debug = true;
	public static final String TAG = "Arctic";
	public static final float SCENE_WIDTH = 1920;
	public static final float SCENE_HEIGHT = 800;
}
