package ru.atonica.arctic;

import android.util.Log;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 04.04.13
 * Time: 14:39
 * To change this template use File | Settings | File Templates.
 */
public class Aurora extends AnimatedSprite implements AnimatedSprite.IAnimationListener {

    private long frameDurationEach;

    public Aurora(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
    }

    public void animateInCicle(final long pFrameDurationEach) {
        frameDurationEach = pFrameDurationEach;
        this.registerUpdateHandler(new TimerHandler(MathUtils.random(5, 20), new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                Aurora.this.animate(pFrameDurationEach, Aurora.this);
            }
        }));

    }

    @Override
    public void stopAnimation() {
        this.clearUpdateHandlers();
        super.stopAnimation();
    }

    @Override
    public void stopAnimation(int pTileIndex) {
        this.clearUpdateHandlers();
        super.stopAnimation(pTileIndex);
    }

    public void animateWithReverse(final long pFrameDurationEach) {
        frameDurationEach = pFrameDurationEach;
        final int tileCount = getTileCount();
        final int doubledTileCount = tileCount * 2 - 1;
        long[] frameDuration = new long[doubledTileCount];
        int[] frames = new int[doubledTileCount];
        int frameCounter = 0;
        boolean reverse = false;
        for(int i=0;i<doubledTileCount;i++) {
            frameDuration[i] = frameDurationEach;
            frames[i] = frameCounter;
            if(frameCounter == tileCount-1) {
                reverse = true;
//                frameDuration[i] = frameDurationEach * 2;
            }
            if(reverse) frameCounter--;
            else frameCounter++;
        }
        animate(frameDuration, frames, this);
    }

    @Override
    public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
    }

    @Override
    public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
    }

    @Override
    public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
    }

    @Override
    public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) {
        final float newXPosition = MathUtils.random(WallpaperSettings.getInstance().camera.getXMin(), WallpaperSettings.getInstance().camera.getXMax());
        final float newYPosition = MathUtils.random(Constants.SCENE_HEIGHT, 570);
        setPosition(newXPosition, newYPosition);
        stopAnimation();
        registerUpdateHandler(new TimerHandler(MathUtils.random(30, 90), new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                animateInCicle(frameDurationEach);
            }
        }));
    }
}
