package ru.atonica.arctic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 21.03.13
 * Time: 13:38
 * To change this template use File | Settings | File Templates.
 */
public class SettingsFragment extends PreferenceFragment {

    Activity a;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        a = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName(WallpaperSettings.PREFERENCES_NAME);
        addPreferencesFromResource(R.xml.walpaper_prefs);
        if(a != null) {
            PreferenceManager.setDefaultValues(a, R.xml.walpaper_prefs, false);
        }
        Preference shareThis = findPreference("share_this");
        shareThis.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Arctic HD Live Wallpaper");
                intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=ru.atonica.arctic");
                startActivity(Intent.createChooser(intent, "Share via"));
                return true;
            }
        });
        final TimePickerPreference sunriseTimePreference = ((TimePickerPreference)findPreference("sunrise_time"));
        sunriseTimePreference.setHoursLimit(4, 10);
        final TimePickerPreference sunsetTimePreference = ((TimePickerPreference)findPreference("sunset_time"));
        sunsetTimePreference.setHoursLimit(16, 23);
        ListPreference sceneTypePreference = (ListPreference)findPreference("scene_type");
        sceneTypePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (newValue.toString().equals("AUTO")) {
                    sunriseTimePreference.setEnabled(true);
                    sunsetTimePreference.setEnabled(true);
                } else {
                    sunriseTimePreference.setEnabled(false);
                    sunsetTimePreference.setEnabled(false);
                }
                return true;
            }
        });
        if(sceneTypePreference.getValue().equals("AUTO")) {
            sunriseTimePreference.setEnabled(true);
            sunsetTimePreference.setEnabled(true);
        } else {
            sunriseTimePreference.setEnabled(false);
            sunsetTimePreference.setEnabled(false);
        }
    }
}
