package ru.atonica.arctic;

import android.content.Context;
import org.andengine.engine.Engine;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder;
import org.andengine.opengl.texture.bitmap.AssetBitmapTexture;
import org.andengine.opengl.texture.bitmap.BitmapTextureFormat;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.util.debug.Debug;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 26.03.13
 * Time: 11:23
 * To change this template use File | Settings | File Templates.
 */
public class ResourceManager {

    private final static ResourceManager INSTANCE = new ResourceManager();

    public static ResourceManager getInstance() {
        return INSTANCE;
    }

    public ResourceManager() {
    }

    private boolean dayTexturesLoaded = false;
    private boolean nightTexturesLoaded = false;
    private boolean sharedTexturesLoaded = false;

    public Engine engine;
    public Context context;

//    Day textures
    public static ITextureRegion dayLittleCloudTextureRegion;
    public static ITextureRegion dayMediumCloudTextureRegion;
    public static ITextureRegion dayBigCloudTextureRegion;
    public static ITextureRegion dayFanTextureRegion;
    public static ITextureRegion dayAirshipTextureRegion;
    public static ITextureRegion dayBackgroundTextureRegion;

    public static ITiledTextureRegion dayWaterTextureRegion;
    public static ITiledTextureRegion daySnowTextureRegion;
    public static ITiledTextureRegion dayBirdTextureRegion;

//    Night textures
    public static ITextureRegion nightAirshipTextureRegion;
    public static ITextureRegion nightBigCloudTextureRegion;
    public static ITextureRegion nightMediumCloudTextureRegion;
    public static ITextureRegion nightFanTextureRegion;
    public static ITextureRegion nightStarTextureRegion;
    public static ITextureRegion nightSkyTextureRegion;
    public static ITextureRegion nightHillsTextureRegion;

    public static ITiledTextureRegion nightAuroraTextureRegion;
    public static ITiledTextureRegion nightSnowTextureRegion;
    public static ITiledTextureRegion nightWaterTextureRegion;
    public static ITiledTextureRegion nightBirdTextureRegion;
    public static ITiledTextureRegion grampusTextureRegion;

//    Shared textures
    public static ITiledTextureRegion smokeTextureRegion;



    public void setup(final Engine pEngine, final Context pContext) {
        engine = pEngine;
        context = pContext;
    }

    public static void loadDayResources() {
        getInstance().loadSharedTextures();
        getInstance().loadDayTextures();
    }

    public static void unloadDayResources() {
        getInstance().unloadDayTextures();
    }

    public static void loadNightResources() {
        getInstance().loadSharedTextures();
        getInstance().loadNightTextures();
    }

    public static void unloadNightResources() {
        getInstance().unloadNightTextures();
    }

    public static void unloadSharedResources() {
        getInstance().unloadSharesTextures();
    }

    private void loadDayTextures() {
        if(dayTexturesLoaded) return;

        try {
            ITexture texture = new AssetBitmapTexture(engine.getTextureManager(), context.getAssets(), "gfx/day/background.png", TextureOptions.BILINEAR);
            dayBackgroundTextureRegion = TextureRegionFactory.extractFromTexture(texture);
            texture.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ITexture texture = new AssetBitmapTexture(engine.getTextureManager(), context.getAssets(), "gfx/day/water.jpg", TextureOptions.BILINEAR);
            dayWaterTextureRegion = TextureRegionFactory.extractTiledFromTexture(texture, 2, 8);
            texture.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ITexture texture = new AssetBitmapTexture(engine.getTextureManager(), context.getAssets(), "gfx/day/snow.png");
            daySnowTextureRegion = TextureRegionFactory.extractTiledFromTexture(texture, 3, 1);
            texture.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/day/");
        BuildableBitmapTextureAtlas buildableBitmapTextureAtlas = new BuildableBitmapTextureAtlas(engine.getTextureManager(), 1024, 1024, BitmapTextureFormat.RGBA_8888, TextureOptions.BILINEAR);

        dayAirshipTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(buildableBitmapTextureAtlas, context, "airship.png");
        dayFanTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(buildableBitmapTextureAtlas, context, "fan.png");
        dayLittleCloudTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(buildableBitmapTextureAtlas, context, "cloud_little.png");
        dayMediumCloudTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(buildableBitmapTextureAtlas, context, "cloud_medium.png");
        dayBigCloudTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(buildableBitmapTextureAtlas, context, "cloud_big.png");
        dayBirdTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(buildableBitmapTextureAtlas,context, "bird.png", 9, 1);

        try {
            buildableBitmapTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 4, 1));
            buildableBitmapTextureAtlas.load();
        } catch (ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            e.printStackTrace();
        }
        dayTexturesLoaded = true;
    }

    private void unloadDayTextures() {
        unloadTexture(dayBackgroundTextureRegion);
        unloadTexture(dayAirshipTextureRegion);
        unloadTexture(dayFanTextureRegion);
        unloadTexture(dayLittleCloudTextureRegion);
        unloadTexture(dayMediumCloudTextureRegion);
        unloadTexture(dayBigCloudTextureRegion);
        unloadTexture(dayWaterTextureRegion);
        unloadTexture(daySnowTextureRegion);
        unloadTexture(dayBirdTextureRegion);
        dayTexturesLoaded = false;
    }

    private void loadNightTextures() {
        if(nightTexturesLoaded) return;

        try {
            ITexture texture = new AssetBitmapTexture(engine.getTextureManager(), context.getAssets(), "gfx/night/sky.jpg", TextureOptions.BILINEAR);
            nightSkyTextureRegion = TextureRegionFactory.extractFromTexture(texture);
            texture.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ITexture texture = new AssetBitmapTexture(engine.getTextureManager(), context.getAssets(), "gfx/night/hills.png", TextureOptions.BILINEAR);
            nightHillsTextureRegion = TextureRegionFactory.extractFromTexture(texture);
            texture.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ITexture texture = new AssetBitmapTexture(engine.getTextureManager(), context.getAssets(), "gfx/night/water.jpg", TextureOptions.BILINEAR);
            nightWaterTextureRegion = TextureRegionFactory.extractTiledFromTexture(texture, 2, 8);
            texture.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ITexture texture = new AssetBitmapTexture(engine.getTextureManager(), context.getAssets(), "gfx/day/snow.png");
            nightSnowTextureRegion = TextureRegionFactory.extractTiledFromTexture(texture, 3, 1);
            texture.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ITexture texture = new AssetBitmapTexture(engine.getTextureManager(), context.getAssets(), "gfx/night/aurora.png", TextureOptions.BILINEAR);
            nightAuroraTextureRegion = TextureRegionFactory.extractTiledFromTexture(texture, 10, 5);
            texture.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/night/");
        BuildableBitmapTextureAtlas atlas = new BuildableBitmapTextureAtlas(engine.getTextureManager(), 1024, 1024, BitmapTextureFormat.RGBA_8888, TextureOptions.BILINEAR);
        nightAirshipTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, context, "airship.png");
        nightBigCloudTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, context, "cloud_big.png");
        nightMediumCloudTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, context, "cloud_medium.png");
        nightFanTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, context, "fan.png");
        nightStarTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, context, "star.png");
        nightBirdTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(atlas,context, "bird.png", 9, 1);

        try {
            atlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 4, 1));
            atlas.load();
        } catch (ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            e.printStackTrace();
        }
        nightTexturesLoaded = true;
    }

    private void unloadNightTextures() {
        unloadTexture(nightAirshipTextureRegion);
        unloadTexture(nightAuroraTextureRegion);
        unloadTexture(nightBigCloudTextureRegion);
        unloadTexture(nightMediumCloudTextureRegion);
        unloadTexture(nightFanTextureRegion);
        unloadTexture(nightHillsTextureRegion);
        unloadTexture(nightSkyTextureRegion);
        unloadTexture(nightSnowTextureRegion);
        unloadTexture(nightStarTextureRegion);
        unloadTexture(nightWaterTextureRegion);
        unloadTexture(nightBirdTextureRegion);
        nightTexturesLoaded = false;
    }

    private void loadSharedTextures() {
        if(sharedTexturesLoaded) return;
/*

        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
        BuildableBitmapTextureAtlas buildableBitmapTextureAtlas = new BuildableBitmapTextureAtlas(engine.getTextureManager(), 1024, 1024, BitmapTextureFormat.RGBA_8888, TextureOptions.BILINEAR);

        smokeTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(buildableBitmapTextureAtlas, context, "smoke.png", 4, 6);

        try {
            buildableBitmapTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 4, 1));
            buildableBitmapTextureAtlas.load();
        } catch (ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            e.printStackTrace();
        }

*/

        try {
            ITexture texture = new AssetBitmapTexture(engine.getTextureManager(), context.getAssets(), "gfx/night/grampus.png", TextureOptions.BILINEAR);
            grampusTextureRegion = TextureRegionFactory.extractTiledFromTexture(texture, 30, 1);
            texture.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ITexture texture = new AssetBitmapTexture(engine.getTextureManager(), context.getAssets(), "gfx/smoke.png", TextureOptions.BILINEAR);
            smokeTextureRegion = TextureRegionFactory.extractTiledFromTexture(texture, 19, 2);
            texture.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        sharedTexturesLoaded = true;
    }

    private void unloadSharesTextures() {
        unloadTexture(smokeTextureRegion);
        unloadTexture(grampusTextureRegion);
        sharedTexturesLoaded = false;
    }

    private void unloadTexture(ITextureRegion pTextureRegion) {
        if(pTextureRegion != null) {
            if(pTextureRegion.getTexture().isLoadedToHardware()) {
                pTextureRegion.getTexture().unload();
                pTextureRegion = null;
            }
        }
    }




}
