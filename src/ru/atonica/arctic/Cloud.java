package ru.atonica.arctic;

import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 26.03.13
 * Time: 11:16
 * To change this template use File | Settings | File Templates.
 */
public class Cloud extends Sprite {
    private final PhysicsHandler mPhisicsHandler;

    public Cloud(float pX, float pY, float pVelocityX, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
        mPhisicsHandler = new PhysicsHandler(this);
        this.registerUpdateHandler(mPhisicsHandler);
        mPhisicsHandler.setVelocity(pVelocityX, 0f);
    }

    @Override
    protected void onManagedUpdate(float pSecondsElapsed) {
        if(this.mX > Constants.SCENE_WIDTH+this.getWidth()/2) {
            this.mX = -this.getWidth()/2;
        }
        super.onManagedUpdate(pSecondsElapsed);
    }

}
