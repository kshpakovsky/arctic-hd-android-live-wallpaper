package ru.atonica.arctic;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.CardinalSplineMoveModifier;
import org.andengine.entity.modifier.CardinalSplineMoveModifier.CardinalSplineMoveModifierConfig;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;
import org.andengine.util.modifier.IModifier;

import android.util.Log;

public class Bird extends AnimatedSprite implements IEntityModifierListener {

    public static final String TAG = "Arctic LWP";
    public final static int MAX_PATH_NUMBER = 0;

    private int pathNumber = 0;
    private int birdDelay = 0;
    private boolean readyToGo = false;

    private float randomXFrom = -25f,
            randomXTo = 25f,
            randomYFrom = -15f, randomYTo = 15f;

    IBirdFinishedListener birdFinishedListener;

	public Bird(float pX, float pY, int pBirdDelay, int pPathNumber, ITiledTextureRegion pTiledTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager) {
		super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
        pathNumber = pPathNumber;
        birdDelay = pBirdDelay;
		this.animate(MathUtils.random(105, 135));
        this.setIgnoreUpdate(true);
        this.registerEntityModifier(getModifier());
        readyToGo = true;
	}

    public void setBirdDelay(int birdDelay) {
        this.birdDelay = birdDelay;
    }

    public void setPathNumber(int pathNumber) {
        this.pathNumber = pathNumber;
    }

    public void setRandomize(final float pRandomizeXFrom, final float pRandomizeXTo, final float pRandomizeYFrom, final float pRandomizeYTo) {
        randomXFrom = pRandomizeXFrom;
        randomXTo = pRandomizeXTo;
        randomYFrom = pRandomizeYFrom;
        randomYTo = pRandomizeYTo;
        this.clearEntityModifiers();
        this.registerEntityModifier(getModifier());
    }

    private CardinalSplineMoveModifierConfig getModConfig(float[][] points, float tension) {
		final int pointsCount = points.length;
		CardinalSplineMoveModifierConfig modConfig = new CardinalSplineMoveModifierConfig(pointsCount, tension);
		for(int i = 0; i < pointsCount; i++) {
			modConfig.setControlPoint(i, points[i][0], points[i][1]);
		}
		return modConfig;
	}
	
	private float[][] randomizePoints(float[][] points) {
		final int pointsCount = points.length;
		float[][] newPoints = new float[pointsCount][2];
		for(int i = 0; i < pointsCount; i++) {
			newPoints[i][0] = points[i][0] + MathUtils.random(randomXFrom, randomXTo);
			newPoints[i][1] = points[i][1] + MathUtils.random(randomYFrom, randomYTo);
		}
		return newPoints;
	}
	
	private SequenceEntityModifier getPath0() {
		float[][] points = {
				{1920, 540},
				{1720, 610},
				{1520, 586},
				{1320, 528},
				{1120, 480},
				{920, 570},
				{720, 606},
				{520, 674},
				{320, 582},
				{120, 650},
				{-250, 730}
		};
		this.setScale(0.5f);
		CardinalSplineMoveModifierConfig modConfig = getModConfig(randomizePoints(points), 0);
		SequenceEntityModifier eMod = new SequenceEntityModifier(
				new DelayModifier(birdDelay),
				new ParallelEntityModifier(
						new ScaleModifier(46, 0.5f, 0.5f),
						new CardinalSplineMoveModifier(50, modConfig)
						)
				);
		eMod.addModifierListener(this);
		eMod.setAutoUnregisterWhenFinished(true);
		return eMod;
	}
	
	private SequenceEntityModifier getPath1() {
		float [][] points = {
				{423, 800},
				{591, 729},
				{725, 644},
				{776, 536},
				{793, 481},
				{800, 469},
				{811, 440},
				{810, 421},
				{810, 403},
				{809, 389},
				{809, 376},
				{809, 370},
				{808, 365}
		};
		this.setScale(0.8f);
		this.setPosition(423, 800);
		CardinalSplineMoveModifierConfig modConfig = getModConfig(randomizePoints(points), 0);
		SequenceEntityModifier eMod = new SequenceEntityModifier(
				new DelayModifier(birdDelay),
				new ParallelEntityModifier(
						new SequenceEntityModifier(
							new ScaleModifier(10, 0.8f, 0.5f),
							new ScaleModifier(7, 0.5f, 0.1f)
							),
						new CardinalSplineMoveModifier(17, modConfig)
						)
				);
		eMod.addModifierListener(this);
		eMod.setAutoUnregisterWhenFinished(true);
		return eMod;
	}
	
	private SequenceEntityModifier getModifier() {
		switch(pathNumber) {
			case 0:
				return getPath0();
			case 1:
				return getPath1();
			default:
				return getPath0();
		}
	}

    public void startBird() {
        readyToGo = false;
        this.setIgnoreUpdate(false);
    }

    public boolean isReadyToGo() {
        return readyToGo;
    }

    @Override
    public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
        pItem.setIgnoreUpdate(true);
        pItem.registerEntityModifier(getModifier());
        readyToGo = true;
        if(birdFinishedListener != null) {
            birdFinishedListener.onBirdFinished();
        }
    }

    @Override
    public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {

    }
	
	private float lastX = this.getX();

	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		if(lastX < this.getX()) {
			this.setFlippedHorizontal(true);
		} else {
			this.setFlippedHorizontal(false);
		}
		lastX = this.getX();
		super.onManagedUpdate(pSecondsElapsed);
	}

    public void setBirdFinishedListener(IBirdFinishedListener pBirdFinishedListener) {
        birdFinishedListener = pBirdFinishedListener;
    }

    public static interface IBirdFinishedListener {
        public void onBirdFinished();
    }
}
