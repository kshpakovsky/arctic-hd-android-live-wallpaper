package ru.atonica.arctic;

import org.andengine.entity.sprite.AnimatedSprite;

/**
 * Created with IntelliJ IDEA.
 * User: sgray
 * Date: 26.03.13
 * Time: 15:41
 * To change this template use File | Settings | File Templates.
 */
public class DayScene extends ManagedScene implements Snow.ISnowEvents {

    private DayScene thisDayScene = this;
    private BirdsFlock birdsFlock;
    private Snow snow;
    private Fan fan;
    private AnimatedSprite smoke;

    @Override
    public void onHideScene() {

    }

    @Override
    public void onLoadScene() {
        ObjectFactory.setSceneType(WallpaperSettings.SceneType.DAY);
        ResourceManager.loadDayResources();

        // water >>
        this.attachChild(ObjectFactory.createWater());
        // << water

        // background >>
        this.attachChild(ObjectFactory.createBackground());
        // << background

        this.attachChild(ObjectFactory.createGrampus());

        // clouds >>
        this.attachChild(ObjectFactory.createLittleCloud(10f));
        this.attachChild(ObjectFactory.createMediumCloud(7f));
        this.attachChild(ObjectFactory.createBigCloud(6f));
        // << clouds

        // fan >>
        fan = ObjectFactory.createFan();
        this.attachChild(fan);
        // << fan

        this.attachChild(ObjectFactory.createAirship());

        // smoke >>
        smoke = ObjectFactory.createSmoke();
        this.attachChild(smoke);
        // << smoke

        birdsFlock = ObjectFactory.createBirdsFlock(WallpaperSettings.getInstance().getBirdsInFlockCount());
        this.attachChild(birdsFlock);

        snow = ObjectFactory.createSnow(null);
        this.attachChild(snow);
    }

    @Override
    public void onShowScene() {

    }

    @Override
    public void onUnloadScene() {
        ResourceManager.getInstance().engine.runOnUpdateThread(new Runnable() {
            @Override
            public void run() {
                thisDayScene.detachChildren();
                thisDayScene.clearEntityModifiers();
                thisDayScene.clearUpdateHandlers();
                ResourceManager.unloadDayResources();
            }
        });
    }

    @Override
    public void onSettingsChanged(String key) {
        if(key.equals(WallpaperSettings.BIRDS_IN_FLOCK_KEY)) {
            if(birdsFlock != null) {
                birdsFlock.detachSelf();
            }
            birdsFlock = ObjectFactory.createBirdsFlock(WallpaperSettings.getInstance().getBirdsInFlockCount());
            this.attachChild(birdsFlock);
        }

        if(key.equals(WallpaperSettings.SNOW_ENABLED_KEY) || key.equals(WallpaperSettings.SNOW_FREQUENCY_KEY)) {
            if(snow != null) {
                snow.setIsSnowy(WallpaperSettings.getInstance().isSnowEnabled());
            }
        }
    }

    @Override
    public void snowStarted() {
        fan.setAngularVelocity(WallpaperSettings.FAN_VELOCITY_FAST);
        smoke.animate(WallpaperSettings.SMOKE_FRAME_DURATION_FAST);
    }

    @Override
    public void snowStopped() {
        fan.setAngularVelocity(WallpaperSettings.FAN_VELOCITY_NORMAL);
        smoke.animate(WallpaperSettings.SMOKE_FRAME_DURATION_NORMAL);
    }
}
